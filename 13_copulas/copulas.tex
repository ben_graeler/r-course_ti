\documentclass[compress,mathserif,table,href,aspectratio=169,8pt]{beamer}

\usepackage{animate}
\usepackage[numbers]{natbib}

\input{../talk-style.tex}

\graphicspath{{../12_Geostatistik/figures/}}

\lecture[]{Copulas}

\date{Thünen-Institut, Braunschweig, 18+19/25+26 Februar 2019}
\author{Dr. Benedikt Gräler}

\begin{document}

\begin{frame}
\maketitle
\end{frame}		

\section{Copulas}

%\begin{frame}[allowframebreaks]{Copulas are everywhere - the bivariate case}
%\includegraphics[width=\textwidth]{students}
%\end{frame}

\begin{frame}[allowframebreaks]{Bivariate Copulas}
Copulas allow to model dependencies much more detailed than a typical correlation value. \vspace{4pt}

Instead of a single value, a full distribution is fitted describing dependence.

\begin{figure}
\includegraphics[width=0.5\textwidth]{normSmpl}
\includegraphics[width=0.5\textwidth]{copGumbSmpl}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{expSmpl}
\includegraphics[width=0.5\textwidth]{mixSmpl}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{normStudentSmpl}
\includegraphics[width=0.5\textwidth]{copStudentSmpl}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
\frametitle{The Fr\'{e}chet-Hoeffding bounds}
\begin{example}
\begin{itemize}\label{exp_bounds}
\item \alert{The Fr\'{e}chet-Hoeffding bounds} \index{Fr\'{e}chet-Hoeffding bounds} \\
For $M(u,v) := \min(u,v)$ \label{M} and $W(u,v) := \max(u+v-1,0)$ \label{W} the following inequality holds for every copula C:
$$
W(u,v) \leq C(u,v) \leq M(u,v) 
$$
\item \alert{The product copula} \label{Pi} \\
 The 2-place function $\Pi$ defined by $\Pi(u,v) := uv $ is a copula.
\end{itemize}
\end{example}
\break

\begin{figure}[h]
\includegraphics[width=100mm]{contourplot_M_Pi_W2-png}
\caption[contour plots of $M$, $\Pi$ and $W$]{Contour plots of $M$, $\Pi$ and $W$ for the level set $\{0,0.1,\hdots,0.9\}$. The light grey triangle represents the Fr\'{e}chet-Hoeffding bounds for $a_0=0.3$.}
\label{plot_contour_M_Pi_W}
\end{figure}

\begin{figure}[h]
\includegraphics[width=100mm]{3D-plot_M_Pi_W.png}
\end{figure}
\end{frame}

\begin{frame}

\frametitle{Sklar's Theorem}
\begin{theorem} \label {sklar00} \index{Sklar's Theorem}
Let $X$ and $Y$ be random variables with distribution functions $F$ and $G$ respectively and joint distribution function $H$. Then there exists a copula $C$ such that for all $(x,y) \in \cIR \times \cIR$:
$$ H(x,y)=C\big(F(x),G(y) \big) $$
$C$ is unique if $F$ and $G$ are continuous; otherwise, $C$ is uniquely determined on $\ran(F) \times \ran(G)$. \\ 
Conversely, if $C$ is a copula and $F$ and $G$ are distribution functions then the function $H$ defined as above is a joint distribution function with margins $F$ and $G$.
\end{theorem}
\end{frame}

\begin{frame}{Why is this useful?}

Sklar's theorem allows us to model any multivariate distribution in two steps:
\begin{enumerate}
\item<1-> find marginal distribution functions using your favourite estimation technique that suite the data
\item<1-> find a copula that describes the dependence
\end{enumerate}

This allows for a huge flexibility and a clear outline how to proceed.\vspace{4pt}
\end{frame}


\begin{frame}[allowframebreaks,fragile]
\frametitle{The Gaussian Copula}
\begin{example} [of Sklar's Theorem]
Consider a bivariate standard Gaussian $(X,Y)$ random variable with mean $\mu := (0,0)$, a correlation $\rho$ and covariance matrix
$\Sigma := \left( 
\begin{array}{cc}
1 & \rho \\
\rho & 1
\end{array}\right).
$ \\
We denote its distribution function by $H_\rho(x,y)$. The margins $X$  and $Y$ posses univariate standard Gaussian distributions $N(0,1)$ with distribution function  $\Phi$. Following Sklar's Theorem we define:
$$ C^N_\rho(u,v) := H_\rho\big(\Phi^{-1}(u),\Phi^{-1}(v)\big) $$
This is the definition of the \alert{Gaussian Copula} $C^N_\rho$ with parameter $\rho$.
\end{example}
\break 

\Note $C^N_\rho$ is as well the copula of \alert{any} bivariate non-standard Gaussian (i.e. $\mu_x, \mu_y \neq 0$, $\sigma_x, \sigma_y \neq 1$) random variable and \alert{many} non Gaussian random variables as well. \\ \vspace{12pt}
\Atte There are "more" bivariate random variables having Gaussian margins but \alert{do not} posses a Gaussian dependence structure (a Gaussian copula).
\end{frame}

\begin{frame}
\frametitle{Copulas \& dependence}
A copula $C: \us \rightarrow \ui$  can be understood as bivariate joint distribution function of some distribution over the unit square $\us$. As such, they posses a bivariate density function: 
$$c: \us \rightarrow [0,\infty)$$ \\
\uncover<2->{\alert{This density is what we are really interested in!}\\ \vspace{12pt}
The copula's density reflects the strength of dependence of the two margins.}
\end{frame}

\begin{frame}
\frametitle{A copula's density }
It holds $C(u,v) = \int_{[0,u]\times[0,v]} c(x,y) d(x,y)$

\begin{figure}[h]
\includegraphics[width=98mm]{plot_Gaussian-png.png}
\caption[Gaussian Copula]{Contour plot and 3D density plot of a Gaussian Copula for $\rho=0.2$.}
\label{plot_3D_M_Pi_W}
\end{figure}
\end{frame}


\subsection{Symmetry}
\begin{frame}
\frametitle{About symmetry}
\begin{definition}<1->
We will introduce two kinds of symmetry:
\begin{itemize}
\item<1-> (plain) symmetry: $C(u,v) = C(v,u) \ \forall \ u,v \in \ui$
\item<1-> radial symmetry: $C(u,v) = u+v-1+C(1-u,1-v) \ \forall \ u,v \in \ui$
\end{itemize}
\end{definition}

\begin{example}<2->
\begin{itemize}
\item<2-> The product copula $\Pi(u,v)=u v$ is (obviously) symmetric.
\item<2->  The Gaussian Copula $C^N_\rho$ is radial symmetric, as any copula deduced from an \alert{elliptical distribution}. 
\end{itemize}
\end{example}
\end{frame}

\begin{frame}
\frametitle{The problem of symmetry}
\begin{itemize}
\item symmetry is nice as long as your process is symmetric
\item<2-> there are (many) natural processes that posses an asymmetric dependence structure 
\begin{itemize}
\item<2-> elevation: valleys are usually smoother than mountains
\item<2-> amount of toxics: the increase of a toxic (due a sudden release) is usually much steeper than its decay
\end{itemize}
\item<3-> unfortunately, most copula families in the literature are symmetric
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{An asymmetric copula}
\begin{example} \label{ASC} \index{copula!asymmetric} \vspace{-18pt}
$$ C^A_{ab}(u,v)=uv+uv(1-u)(1-v)\big((a-b)v(1-u)+b \big)$$ 
for all $|b| \leq 1$ and $\big(b-3-\sqrt{9+6b-3b^2}\big) / 2 \leq a \leq 1$ with $a \neq b$. \\ \vspace{-8pt}
\begin{figure}[h]
\includegraphics[width=98mm]{plot_ASC2-png}
\caption[Asymmetric Copula]{Contour plot and density plot of the asymmetric copula with $a=-0.5$ and $b=0.3$.}
\end{figure}
\end{example}
\end{frame}

\begin{frame}
\frametitle{An asymmetric pseudo-sample}
\begin{figure}
\includegraphics[width=98mm]{smpl_shapes_color}
\caption[Asymmetric data]{An eleven day subset of some asymmetric pseudo-sample.}
\end{figure}
\end{frame}

\subsection{Tail Dependence}
\begin{frame}[allowframebreaks]
\frametitle{Dependencies of extremes}
In cases of extreme events one is interested in the probability to see joint extremes. \\
This is $\IP \left( Y > G^-(t) \big| X > F^-(t) \right)$ for some $t$ close to $1$ or $0$. We define the upper and lower tail dependence:
\begin{align*}
 \lambda_U &= \lim\limits_{t \nearrow 1} \IP \left( Y > G^-(t) \big| X > F^-(t) \right) \\
	&= 2-\lim\limits_{t \nearrow 1} \frac{1-C(t,t)}{1-t} \\
 \lambda_L &= \lim\limits_{t \searrow 0} \IP \left( Y \leq G^-(t) \big| X \leq F^-(t) \right) \\
	&= \lim\limits_{t \searrow 0} \frac{C(t,t)}{t}
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Some examples of tail dependence}
\begin{example}
\begin{itemize}
\item<1-> any radial symmetric copula has equivalent upper and lower tail dependence
\item<1-> the family of Gaussian copulas does not exhibit any tail dependence 
		\\(even Gaussian Copulas with correlation coefficients $\rho$ very close to $1$ generate (almost) independent extremes)
\item<1-> the copulas $W$ (perfect negative dependence) and $\Pi$ (independence) do not exhibit any tail dependence
\item<1-> for the copula $M$ (perfect positive dependence) we get $\lambda_U=\lambda_L=1$
\end{itemize}
\end{example}
\end{frame}

\begin{frame}{Tail dependence - graphically}
\begin{figure}
\includegraphics[height=0.8\textheight]{tailDep}
\end{figure}
\end{frame}

\subsection{Elliptical Copulas}
\begin{frame}[allowframebreaks]
\frametitle{Gaussian Copulas}

We have seen the Gaussian Copula before: 
$$ C^N_\rho(u,v) := \Phi_\rho\big(\Phi^{-1}(u),\Phi^{-1}(v)\big) $$
Its density evaluates to:
$$ c^N_\rho (u,v) = \frac{\varphi_\rho \big( \Phi^{-1}(u),\Phi^{-1}(u) \big)}{\varphi \big(\Phi^{-1}(u) \big) \varphi \big(\Phi^{-1}(v)\big)} $$
With $-1 \leq \rho \leq 1$ (Pearson's correlation coefficient)

\framebreak

\begin{figure}[h]
\includegraphics[width=98mm]{plot_Gaussian-png.png}
\caption[Gaussian Copula]{Contour plot and 3D density plot of a Gaussian Copula for $\rho=0.2$.}
\label{plot_3D_M_Pi_W}
\end{figure}
\end{frame}

\begin{frame}[fragile, allowframebreaks]
\frametitle{Student Copulas}
The Student Copula (or t-Copula) is derived from the t-distribution:
$$
 C^t_{\nu,\rho}(u,v)=t_{\nu,\rho} \big( t_\nu^{-1}(u),t_\nu^{-1}(v) \big)
$$
Where $t_{\nu,\rho}$ is the cumulative distribution function of a bivariate $t_{\nu,\rho}$ distribution and $\rho$ is the correlation coefficient.\\ 
Its density evaluates to:
$$
 c^t_{\nu,\rho}(u,v) = \frac{f_{\nu,\rho} \big( f_\nu \big( t_\nu^{-1}(u) \big),f_\nu \big( t_\nu^{-1}(v) \big) \big)} {f_\nu \big( t_\nu^{-1}(u) \big) f_\nu \big( t_\nu^{-1}(v) \big) }
$$
Where $f_{\nu,\rho}$ is the joint density of a bivariate $t_\nu$-distribution.
\framebreak
\begin{figure}
\includegraphics[width=98mm]{plot_tCopula-png}  
\caption[t-Copula]{Contour plot and density plot of a t-Copula with $\rho=0.2$ and $\nu=1$. The density graph is limited to a level of $10$ (the values for $(u,v)=(0,0)$ and $(u,v)=(1,1)$ reach up to $24.2$).}
\end{figure}
\framebreak
A t-Copula's tail dependence can be evaluated by
$$ 
\lambda^t_{\nu,\rho}=2t_{\nu+1} \left(\frac{-\sqrt{(1+\nu)(1-\rho)}}{\sqrt{1+\rho}} \right).
$$
Surprisingly, a t-Copula exhibits tail dependence even for negative correlation coefficients.
\framebreak
\begin{figure}
\includegraphics[width=98mm]{tailDep_tCop-png}  
\caption[Tail dependencies of different t-Copulas]{Comparison of the relation of the linear correlation parameter $\rho$ and the tail dependence $\lambda$ for different values of $\nu$.}
\end{figure}

\end{frame}

\subsection{Archimedean Copulas}
\begin{frame}
\frametitle{Archimedean Copulas}
A vast and flexible class of copulas are the \alert{Archimedean Copulas}. They are defined by:
\begin{definition}
 $C(u,v) = \varphi^{[-]}(\varphi(u)+\varphi(v))$ is an \alert{Archimedean Copula} \index{copula!Archimedean} for any strictly decreasing convex function $\varphi$ with $\varphi(1)=0$ -  its \alert{generator}. \index{generator}
$\varphi^{[-]}$ is defined as the \alert{pseudo-inverse} of $\varphi$: 
$$ \varphi^{[-]}(t):= \left\{ 
  \begin{array}{ll}
    \varphi^{-1}(t) & , if \ 0 \leq t \leq \varphi(0) \\
    0 & , if \ \varphi(0) \leq t \leq \infty
  \end{array}
\right. $$
\end{definition}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
\frametitle{Some explicit Archimedean Copulas}
\begin{example}
The \alert{Frank family} $\FF := \{C^F_\theta | \theta \in \Theta_F \}$: \index{copula!Frank} \label{frnk} \\
For any parameter $\theta \in \Theta_F := (-\infty,\infty) \backslash \{0\}$ and the corresponding generator $\varphi^F_\theta(t)=-\ln(\frac{\exp(-\theta t) - 1}{\exp(-\theta) - 1})$ one achieves
$$ 
 C^F_\theta(u,v)=-\tfrac{1}{\theta} \ln \left( {1+\frac {(e^{-\theta u}-1)(e^{-\theta v}-1)}{e^{-\theta}-1} } \right).
$$ 
They posses the lower and the upper Fr\a'{e}chet-Hoeffding bounds as limiting cases as $\theta$ approaches $-\infty$ and $\infty$ respectively. For $\theta \rightarrow \pm 0$ it takes the product copula as its limit $C_{F,\pm0}=\Pi$. For all Frank copulas $ C^F_\theta(u,v)=\hat{C}^F_\theta(u,v)=u+v-1+C^F_\theta(1-u,1-v) $ holds and it is $\lambda^F_U=\lambda^F_L=0$. This family is the only Archimedean radially symmetric one.
\end{example}
\break
\begin{figure}
\includegraphics[width=98mm]{plot_Frank}  
\caption[Frank Copula]{Contour plot and density plot of a Frank Copula $C^F_7$.}
\end{figure}
\break
\begin{example}
The \alert{Gumbel family} $\GF := \{C^G_\theta | \theta \in \Theta_G \}$: \index{copula!Gumbel}\label{gumb} \\
For a parameter $\theta \in \Theta_G := [1,\infty)$ and the generator $\varphi^G_\theta(t)=(-\ln(t))^\theta$ one achieves 
$$
C^G_\theta(u,v)= \exp \left( - \left( (-\ln (u))^\theta + (-\ln(v))^\theta  \right)^{1/ \theta} \right)
$$
These copulas range from the product copula $\Pi$ for $\theta=1$ to the upper Fr\a'{e}chet-Hoeffding bound as limiting case while $\theta$ approaches $\infty$. \\ The tail dependence parameters evaluate to $\lambda^G_U=2-2^{1/\theta}$ and $\lambda^G_L=0$.
\end{example}
\break
\begin{figure}
\includegraphics[width=98mm]{plot_Gumbel}  
\caption[Gumbel Copula]{Contour plot and density plot of a Gumbel Copula $C^G_3$.}
\end{figure}
\break
\begin{example}
The \alert{Clayton family} $\CF := \{C^C_\theta | \theta \in \Theta_C \}$: \index{copula!Clayton} \label{clayton} \\
For the parameter space $\Theta_C :=[-1,\infty) \backslash \{0\}$ and generators of the form $\varphi^C_\theta(t)= t^{-\theta}-1$ with $ \theta \in \Theta_C$ one achieves
 $$
 C^C_\theta(u,v)= \left( \max \left( u^{-\theta} + v^{-\theta}-1,0  \right) \right)^{-1 / \theta}.
 $$
 These copulas range from the lower to almost the upper Fr\a'{e}chet-Hoeffding bounds as $\theta$ equals $-1$ or approaches $\infty$ respectively. For $\theta$ tending towards $\pm 0$ the family converges to the product copula $\Pi$. \\
The tail dependence parameters evaluate to $\lambda^C_U=0$ and $\lambda^G_L=2^{-1/\theta}$.
\end{example}
\break
\begin{figure}
\includegraphics[width=98mm]{plot_Clayton}  
\caption[Clayton Copula]{Contour plot and density plot of a Clayton Copula $C^C_1$.}
\end{figure}
\end{frame}

\begin{frame}{Zero Correlation - might carry structure}
\begin{figure}
\center
\includegraphics[width=\textwidth]{asc.pdf}
\end{figure}

Further copulas can be explored at \href{http://copulatheque.org}{copulatheque.org}.
\end{frame}


\section{Estimation of Copulas}

\begin{frame}
\frametitle{Transform of margins}
There are several possibilities to estimate a copula within a family (the choice of family has to be achieved upon inherited properties, by smart guessing or afterwards by GOF-tests). But before, we need to transform the margins by
\begin{itemize}
\item<1-> knowing the marginal distributions
\item<1-> estimating the marginal distributions
\item<1-> approximating the marginal distributions by a rank-order transformation
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{rank-order Transformation}
In the transformed dataset $\tilde{\sZ}$ any observation $z_i \in \sZ$ is replaced by its rank divided by the number of observations+1:
$$ \tilde{\sZ} := \left\{ \frac{\rank(z_i)}{n+1} \big| \ 1 \leq i \leq n \right\}$$

$ \tilde{\sZ}$ is uniformly distributed. 
This approach does not alter the copula as a copula is invariant under strictly increasing transformations of the margins. (= As long as you do not alter the ranks in the sample, you do not alter the copula.)
\end{frame}

\begin{frame}
\frametitle{Empirical Copula}
For a sample $(\sX,\sY)$ with transformed margins the \alert{empirical copula} is defined as:\label{empCop} \index{copula!empirical}
$$
 C_n (u,v)= \frac{ \#\{k \in \{1,\hdots, n\} \big| x_k \leq u \wedge y_k \leq v \} }{n}
$$ 
A two-dimensional step function.

We will denote the \alert{empirical copula frequency (empirical density)} by $c_n$. It is given by:
$$
 c_n\left(\tfrac{i}{n},\tfrac{j}{n}\right)=\left\{ 
  \begin{array}{ll}
    1/n & , if \ (x_{(i)},y_{(j)}) \in (\sXY) \\
      0 & , otherwise
  \end{array}
 \right.
$$
\end{frame}

\begin{frame}
\frametitle{different estimation procedures}
Copulas can be estimated by
\begin{itemize}
\item<1-> a Maximum Likelihood approach
\item<1-> a moment based approach incorporating measures of association like \alert{Kendall's tau} or \alert{Spearman's rho} (does not apply in a general way)
\item<1-> mixtures of both
\item<1-> a Bayesian approach 
\item<1-> others
\end{itemize}
\end{frame}

\subsection{Maximum Likelihood}
\begin{frame}[fragile]
\frametitle{Maximum Likelihood estimation}
Assume a bivariate dataset with uniform distributed margins $\sU=(u_1, \dots, u_n)$ and $\sV=(v_1, \dots, v_n)$. For a given copula family $\mathcal{C}$ with parameter space $\Theta_{\mathcal{C}}$ we define its log-likelihood function by:
\begin{align*}
\mathcal{L}(\theta) &= \sum_{i=1}^n \log \big( c_\theta \left( u,v \right) \big) \\
	 \hat{\theta} &= \arg\max_{\theta \in \Theta} \mathcal{L}(\theta)
\end{align*}
This approach can easily be extend to copulas of higher dimensions.
\end{frame}

\begin{frame}[fragile]
\frametitle{Maximum Liklihood in R}
The library  {\verb=copula=}\footnote{see \url{http://cran.r-project.org/web/packages/copula/}} offers a build-in method \verb=fitCopula()= to estimate copulas. The data needs to be provided as matrix. In order to choose a copula family one member needs to be provided to the function.
\begin{semiverbatim}
fitCopula(copula, data, method="ml")

uranium.biv <- as.matrix(uranium[c("U","Li")]) 
fitCopula(frankCopula(.4),uranium.biv,method="ml")
The estimation is based on the maximum likelihood
and a sample of size 655.
      Estimate   Std. Error  z value Pr(>|z|)
param 1.206623 0.0007958563 1516.131        0
The maximized loglikelihood is  599.156 
\end{semiverbatim}
\end{frame}


\subsection{Moment based}
\begin{frame}[fragile]
\frametitle{Copulas and Kendall's tau and Spearman's rho}
The two measures of association \alert{Kendall's tau} and \alert{Spearman's rho} can be derived from any copula. Some exhibit a nice functional relation between their parameter and one or both measure(s) of association above. \\ \vspace{6pt}
The population version of Kendal's tau is given by:
$$  \tau_C = 4 \int_\us C(u,v) \ \mathrm{d} C(u,v)-1 =_{Arch. Cop.} 1 + 4 \int_0^1 \frac{\varphi(t)}{\varphi'(t)} \ \mathrm{d} t $$
The population version of Spearman's rho is given by:
$$ \rho_{C} = 12 \int_\us C(u,v) -uv \ \mathrm{d}(u,v)$$
\end{frame}

\begin{frame}
\frametitle{estimating Kendall's tau}
\begin{definition}
Let $(\sXY)$ denote the $n$ observations drawn from a continuous random vector $(X,Y)$. We denote the number of concordant pairs by $c$ and of discordant pairs by $d$. The empirical version of Kendall's tau is \label{tau_n}:
$$
 \hat{\tau} \big( \sXY \big) := \frac{c-d}{c+d} = \frac{c-d}{\binom{n}{2}}
$$
 In case the sample contains any ties we use the following corrected version
$$
 \hat{\tau} \big( \sXY \big) := \frac{c-d}{\sqrt{c+d+t_x}\sqrt{c+d+t_y}}.
$$
 Where $t_x$ and $t_y$ are the number of ties in $\sX$ or $\mathrm{\mathbf{Y}}$ only while ties that happen to occur in both margins simultaneously are not counted at all.
\end{definition}
\end{frame}



\begin{frame}
\frametitle{estimating Spearman's rho}
\begin{definition}
 For a given sample $(\sXY)$ of size $n$, drawn from a continuous random vector, we define the empirical version of Spearman's rho by
$$\label{rho_n}
 \hat{\rho} \big( \sXY \big):=1-\frac{6 \sum\limits_{i=1}^n \Delta_i^2}{n(n^2-1)}
$$ where $\Delta_i := \rank(x_i)-\rank(y_i)$ for $(x_i,y_i)\in (\sXY)$, $1 \leq i \leq n$. 
 In case of ties within the sample we consider the averaged ranks and adjust $\hat{\rho}$ by
$$
 \hat{\rho}\big( \sXY \big):=\frac{n\sum(r_i s_i)-\sum r_i \sum s_i}{\sqrt{n \sum r_i^2 - (\sum r_i)^2} \sqrt{n \sum s_i^2 - (\sum s_i)^2}}.
$$ while all sums are taken over $i=1, \hdots, n$. The variables $r_i$ and $s_i$ are given as $r_i:=\rank(x_i)$ and $s_i:=\rank(y_i)$.
\end{definition}

\end{frame}

\begin{frame}
\frametitle{Spearman's rho}

Spearmn's rho can be thought of as the standard correlation coefficient (Pearson) applied to the ranks of a sample. \vspace{4pt}

Spearman's rho assigns $1$ to a perfect monotonic dependence structure which need not be linear in any sense. \vspace{4pt}

In general, it is less sensitive to outliers than Pearson's correlation coefficient. \vspace{4pt}

\end{frame}

\begin{frame}[fragile]
\frametitle{Kendall's tau and Spearman's rho in R}
The function \verb?cor()? provides an argument \verb?method? that takes \verb?"pearson"?, \verb?"kendall"? or \verb?"spearman"?. Where \verb?"pearson"? is the default vlaue. \\
\end{frame}

\begin{frame}
\frametitle{Estimating copulas with $\hat{\tau}$ or $\hat{\rho}$}
\begin{definition}
We define the inverse tau esitmator as
$$
 \hat{\theta}_K=\arg\min\limits_{\theta \in \Theta} \big( \hat{\tau}\big( \sXY \big)-\tau_\theta \big)^2.
$$
\end{definition}
\begin{definition}<1->
We define the inverse rho esitmator as
$$
 \hat{\theta}_S=\arg\min\limits_{\theta \in \Theta} \big( \hat{\rho}\big( \sXY \big)-\rho_\theta \big)^2.
$$
\end{definition}
\Note In cases where $\hat{\tau}$ or $\hat{\rho}$ take values which cannot be represented by a given copula family the estimates might be rather missleading.
\end{frame}

\begin{frame}[fragile]
\frametitle{Some {\em nice} copula families}
\begin{example}
$$
 \hat{\theta}_K\big(\sXY\big):=f\big(\hat{\tau}(\sXY)\big).
$$
 While $f(x)$ takes one of the following forms and we will give $\hat{\theta}_K$ a superscript accordingly:
\setlength{\jot}{6pt}
\begin{align*}
 f_G(x) & :=  1/(1-x), \ 0 \leq x < 1 &\text{Gumbel, } &\Theta_G=[1,\infty) \\
 f_C(x) & :=  2 x /(1-x), \ x < 1 &\text{Clayton, } &\Theta_C=[-1,\infty) \\
 f_N(x) & :=  \sin \left( \tfrac{1}{2} \pi x \right) &\text{Gaussian, } &\Theta_N=[-1,1] \\
 f_t(x) & :=  f_N(x) &\text{Student, } &\Theta_t=[-1,1]
\end{align*}
\end{example}
\end{frame}

\begin{frame}[fragile,squeeze]
\frametitle{Moment based estimator in R}
The function \verb?fitCopula()? provides both estimation methods as well. The argument \verb?method? needs to be changed to \verb?"itau"? or \verb?"irho"? respectively. {\footnotesize
\begin{verbatim}
fitCopula(frankCopula(.4),uranium.biv,method="itau")
The estimation is based on the inversion of Kendall's tau
and a sample of size 655.
      Estimate Std. Error  z value     Pr(>|z|)
param 1.210628  0.3102544 3.902051 9.538113e-05

fitCopula(frankCopula(.4),uranium.biv,method="irho")
The estimation is based on the inversion of Spearman's rho
and a sample of size 655.
      Estimate Std. Error  z value   Pr(>|z|)
param 1.198800  0.6369692 1.882037 0.05983096
\end{verbatim}}
\end{frame}

\subsection{GOF}
\begin{frame}[fragile,allowframebreaks]
\frametitle{GOF for Copulas}
 For an empirical copula $C_n$ and $C_{\hat{\theta}}$ the \alert{Cram\'{e}r-von Mises test-statistic} for $H_0: C = C_{\hat{\theta}}$ is given by:
$$
 S_n := \int_{\us} n \big( C_n (u,v) -C_{\hat{\theta}}(u,v)\big)^2 \mathrm{d}C_n(u,v)
$$
 For numerical evaluation purposes the Riemann sum approximate can be used:
$$
 \tilde{S_n} := \sum\limits_{i=0}^n \big( C_n (u_i,v_i) -C_{\hat{\theta}}(u_i,v_i)\big)^2
$$ Where $\big((u_1,v_1),\hdots,(u_n,v_n)\big)$ is the transformed sample with margins on $(0,1)$.

\framebreak

\alert{Kendall's Cram\'{e}r-von Mises test-statistic} is defined  as: \index{test-statistic!Kendall's CvM} \label{KCvM}
$$ 
 S^K_n := \int_{\ui} n (K_n(v)-K_{\hat{\theta}}(v))^2 \mathrm{d}K_{\hat{\theta}}(v)
$$
 For ease of numerical evaluation its Riemann sum approximate can be used
\begin{eqnarray*}
 \tilde{S^K_n} := \frac{n}{3} 
  & + & n \sum\limits_{i=1}^{n-1} K_n(u_i)^2 \big( K_{\hat{\theta}}(u_{i+1})   - K_{\hat{\theta}}(u_i) \big) \\
  & - & n \sum\limits_{i=1}^{n-1} K_n(u_i)   \big( K_{\hat{\theta}}(u_{i+1})^2 - K_{\hat{\theta}}(u_i)^2 \big)
\end{eqnarray*}

 while $u_1\leq \ \hdots \ \leq u_n$ are the ordered values of $\{V_1,\hdots,V_n \}$, $V_i:=C_n(F_n(x_i),G_n(y_i)), \ i=1,\hdots,n $,
$K_n(v) := \frac{1}{n} \# \left\{ k \in \{1,\hdots,n\} \big| V_k \leq v \right\}, \ v \in \ui$ and 
$K_\theta(t) :=\int_{\us} 1_{C_\theta(u,v) \leq t} \ \mathrm{d} C_\theta(u,v)$.

\framebreak

An approximate p-value can be achieved by:

\begin{enumerate}[i)]
\item
 Estimate $\theta$ from the sample through one of the given estimators and calculate its empirical copula $C_n$ (Kendall distribution $K_n$).
\item 
 Compute the test-statistic $s_0:=\tilde{S}_n$ ($s_0:=\tilde{S}^K_n$). 
\item
 Simulate a sample $\bar{\sZ}$ from the copula $C_\theta$ of the same size as the original one and calculate their corresponding rank statistics.
\item
 Estimate $\bar{\theta}$ from $\bar{\sZ}$ through the same estimator as above and calculate its empirical copula $\bar{C_n}$ (Kendall distribution $\bar{K}_n$).
\item
 Repeat the steps \romannumeral3) \ and \romannumeral4) \ for a large integer $N$ and compute its test-statistic $s_i:=\tilde{S}_{n}$ ($s_i:=\tilde{S}^K_{n}$) for any $1 \leq i \leq N$. 
\item
 The approximate p-value is $\#\{ 1 \leq i \leq N \ | \ s_i > s_o \}/N.$
\end{enumerate}

\end{frame}

\begin{frame}[fragile,allowframebreaks]
\frametitle{Graphical GOF}
A graphical tool to decide which copula fits best can as well be deduced from the Kendall distribution function. We define an empirical and theoretical version of a function $\lambda: \ui \rightarrow [-1,1]$ respectively by
$$
 \lambda_n(v) := v - K_n(v)
$$ and $$
 \lambda_\theta(v) := v - K_\theta(v).
$$
 A comparison of $\lambda_n$ with (maybe multiple) $\lambda_\theta$ in a single plot may give some guidance.

\framebreak

\begin{figure}[bt]
  \includegraphics[width=102mm]{Kn_vs_lambda-png}  
 \caption[graphical GOF distinction]{Comparison of empirical and theoretical Kendall distribution $K(v)$ (left) and $\lambda(v)$ (right).}
 \label{plot_Kn_vs_lambda}
\end{figure}

\end{frame}

\section{Tasks}
\begin{frame}[fragile]
\frametitle{Tasks - In any case: ask your questions!}
\begin{enumerate}[a)]
\item compare Kendall's tau, Spearman's rho and Pearson's correlation coefficient with each other for some bivariate random numbers generated by a copula, and some data set (zinc with lead, ...).
\item plot all three correlation measures for a set of generating parameters ($\approx 10$).
\item estimate a bivariate copula using the \verb?copula? package for the \verb?uranium? dataset, compare different families and choose the best fitting one
\item pick your favourite bivariate data set and explore its scatterplot, select a suitable copula (using \pkg{copula})
\item draw samples from different other fitted families (Gaussian, t, Gumbel, Frank, \dots) and compare the scatter
\end{enumerate}
\end{frame}
\end{document}
