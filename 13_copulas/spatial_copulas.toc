\babel@toc {english}{}
\beamer@sectionintoc {1}{applied copulas}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Rainfall IDF}{7}{0}{1}
\beamer@subsectionintoc {1}{2}{Point pattern counts}{8}{0}{1}
\beamer@subsectionintoc {1}{3}{Biv. Sp. Copula}{9}{0}{1}
\beamer@subsectionintoc {1}{4}{Sp. Vine Copula}{11}{0}{1}
\beamer@subsectionintoc {1}{5}{Software}{15}{0}{1}
\beamer@sectionintoc {2}{Application to Nuclear Radiation}{16}{0}{2}
\beamer@subsectionintoc {2}{1}{Fitment}{16}{0}{2}
\beamer@subsectionintoc {2}{2}{Goodness of Fit}{22}{0}{2}
\beamer@sectionintoc {3}{Spatio-Temporal Copulas}{26}{0}{3}
\beamer@subsectionintoc {3}{1}{Biv. Spatio-Temporal Copulas}{28}{0}{3}
\beamer@subsectionintoc {3}{2}{Spatio-Temporal Vines}{29}{0}{3}
\beamer@sectionintoc {4}{Application}{31}{0}{4}
\beamer@subsectionintoc {4}{1}{Daily Mean $\rm {PM}_{10}$}{31}{0}{4}
\beamer@sectionintoc {5}{Discussion}{40}{0}{5}
\beamer@sectionintoc {6}{Conclusion \& Outlook}{42}{0}{6}
\beamer@sectionintoc {7}{Tasks}{44}{0}{7}
