\documentclass[compress,mathserif,table,href]{beamer}

\usepackage{animate}
\usepackage[numbers]{natbib}
% \newcommand{\newblock}{}

\input{../talk-style.tex}

\graphicspath{{figures/}}

\lecture[4]{Copulas in Spatial Statistics}

\date{2017-07-04}
\rowcolors{1}{}{}
\begin{document}

\begin{frame}
\maketitle
\end{frame}		


\section{applied copulas}

\begin{frame}{What if the world happens to be non-Gaussian?}
\begin{figure}
\includegraphics[width=0.8\textwidth]{ellipSymWireframe}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks]{Multivariate return periods}

Conceptual difference of MRP definitions
\begin{figure}
\center
\includegraphics[width=0.5\textwidth]{hess-2012-224-f02}
\end{figure}

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{hess-2012-224-f06}
\end{figure}

\begin{figure}
\center
\includegraphics[width=\textwidth]{hess-2012-224-f10}
\end{figure}
\end{frame}

\begin{frame}{Alternate spatial copula approaches}

\begin{itemize}
\item Bardossy uses copulas parametrized by their covariance matrix (Gaussian, t, V-transform)
\item Kazianka and Pilz use copulas in a Bayesian setting
\item Erhardt et al. use R-vine copulas to model spatial time series of daily mean temperatures
\end{itemize}
\end{frame}

\subsection{Rainfall IDF}
\begin{frame}{Conditional rainfall distributions}
\begin{figure}
\includegraphics[width=0.9\textwidth]{copComp}
\end{figure}
\end{frame}

\subsection{Point pattern counts}
\begin{frame}{Counts of rat sightings in Madrid}
\begin{figure}
\includegraphics[width=0.9\textwidth]{map_obs_pred_counts}
\end{figure}

\begin{figure}
\includegraphics[width=0.9\textwidth]{cond_density_probability_comp}
\end{figure}
\end{frame}
\subsection{Biv. Sp. Copula}

\begin{frame}{Accounting for distance}
Thinking of pairs of locations $(s_i, s_j)$ we assume \dots
\begin{description}
\item[distance] has a strong influence on the strength of dependence
\item[dependence structure] is identical for all neighbours, but might change with distance
\item[stationarity] and build $k$ bins by spatial distance and estimate a bivariate copula $c_{j}(u,v)$ for each bin $\big\{[0,l_1)$,$[l_1,l_2)$, \dots, $[l_{k-1},l_k)\big\}$
\end{description}
\end{frame}

\begin{frame}{Density of the bivariate spatial copula}
The density of the \alert{bivariate spatial copula} is given by a convex combination of bivariate copula densities:

{\small
$$ c_{h} (u,v) := \left\{
\begin{array}{ll}
c_{1}(u,v) &, 0 \leq h < l_1 \\
(1-\lambda_2) c_{1}(u,v) + \lambda_2 c_{2}(u,v) &, l_1 \leq h < l_2 \\
\vdots & \vdots \\
(1-\lambda_k) c_{k-1}(u,v) + \lambda_k\cdot 1 &, l_{k-1} \leq h < l_k \\
1 &, l_k \leq h 
\end{array}
\right. $$}
where \small{$ \lambda_j := \frac{h-l_{j-1}}{l_j-l_{j-1}} $}.
\vspace{4pt}
Each tree has its own bivariate spatial copula where distance $h$ relates the involved pairs of locations.
\end{frame}

\subsection{Sp. Vine Copula}

\begin{frame}{The spatial neighbourhood}
\begin{figure}
\includegraphics[width=\textwidth]{can_Vine.png}
\end{figure}
\end{frame}

\begin{frame}{The upper trees}
Using the bivariate spatial copula on the first tree, the sample conditioned on $s_0$ is obtained. \vspace{4pt}

The next bivariate spatial copula uses the distances between locations $(s_1,s_2)$, $(s_1,s_3)$, \dots, $(s_1,s_d)$. \vspace{4pt}

A spatial binning allows to estimate the next bivariate spatial copula to generate the sample conditioned on $s_0$ and $s_1$. \vspace{4pt}

$\vdots$
\end{frame}

\begin{frame}{The full density}

We get the full copula density as a product of all involved bivariate densities:

\begin{align*}
& c_{\mathbf{h}} (u_{0},\dots,u_{d}) \nonumber\\
=& \prod\limits_{i=1}^{d} c_{h_0({i})}(u_0,u_i) \cdot \prod\limits_{j=1}^{d-1}\prod\limits_{i=1}^{d-j}c_{h_{j}(j+i)} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1})
\end{align*}
where $u_i = F_i\big(Z(s_i)\big)$ for $0 \leq i \leq d$ and
\begin{align*}
u_{j+i|0,\dots, j-1}&=F_{h_{j-1}(j+i)}(u_{j+i}|u_0,\dots, u_{j-1})\\
&=\frac{\partial C_{h_{j-1}(j+i)}(u_{j-1|0,\dots j-2},u_{j+i|0,\dots j-2})}{\partial u_{j-1|0,\dots j-2}}
\end{align*}\end{frame}

\begin{frame}{Spatial vine copula interpolation}

The estimate can be obtained as the expected value

$$ \widehat{Z}_m(s_0) = \int\limits_{[0,1]}  F^{-1}(u) \ c_{\mathbf{h}}\big(u|u_1,\dots,u_d\big) \ \mathrm{d}u $$

or by calculating any percentile $p$ (i.e. the median)

$$ \widehat{Z}_p(s_0) = F^{-1}\big(C_{\mathbf{h}}^{-1}(p|u_1,\dots,u_d)\big) $$

with the conditional density
$$
c_{\mathbf{h}}(u|u_1,\dots,u_d) := \frac{c_{\mathbf{h}}(u,u_1,\dots,u_d)}{\int_0^1 c_{\mathbf{h}}(v, u_1, \dots, u_d) \mathrm{d}v}
$$
and $u_i = F\big(Z(s_i)\big)$ as before.

\end{frame}

\subsection{Software}
\begin{frame}[fragile]
\frametitle{R-package spcopula}
The developed methods are implemented as R-scripts and are bundled in the package \href{http://r-forge.r-project.org/projects/spcopula/}{spcopula} available at R-Forge. \vspace{4pt}

The package spcopula extends and combines the R-packages \href{http://cran.r-project.org/web/packages/VineCopula/index.html}{VineCopula}, \href{http://cran.r-project.org/web/packages/spacetime/index.html}{spacetime} and \href{http://cran.r-project.org/web/packages/copula/index.html}{copula}. \vspace{4pt}
\end{frame}

\section{Application to Nuclear Radiation}
\subsection{Fitment}
\begin{frame}{Simulated nuclear radiation}
The new method is applied to simulated nuclear radiation data mimicking an emergency scenario. \vspace{4pt}

The data has been generated for the spatial interpolation comparison 2004 (SIC2004) with 200 data and 808 validation locations. \vspace{4pt}

To better approximate stationarity, a quadratic trend surface has been fitted excluding the (two) extremes. \vspace{4pt}

This has been published in Journal of Spatial Statistics.
\end{frame}

\begin{frame}{The trend surface}
\begin{figure}
\center
\includegraphics[scale=0.4]{trendSurf.pdf}
\end{figure}
\end{frame}

\begin{frame}{The marginal distribution}
\begin{figure}
\includegraphics[width=\textwidth]{joker_hist.pdf}
\caption{Histogram of the "observed" radiation values.}
\end{figure}

The empirical marginal distribution function has been used.
\end{frame}

\begin{frame}[allowframebreaks]{The bivariate spatial copulas}
\vspace{-12pt}
\begin{figure}
\center
\animategraphics[loop,scale=0.3,poster=first,autoplay]{5}{animate/plot}{11}{80}
\includegraphics[width=0.75\textwidth]{jokerSpCopula1}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{emergency_spatial_copula}
\end{figure}
\end{frame}

\begin{frame}{Interpolated grid}
\begin{figure}
\includegraphics[width=\textwidth]{quantilePredWireFrame}
\end{figure}
\end{frame}

\subsection{Goodness of Fit}
\begin{frame}\frametitle{Validation data set}
808 locations have been hold back to assess the prediction quality.
\begin{table}
\center

\begin{tabular}{l|rrrr}
approach & MAE & RMSE & ME & COR \\ \hline
spatial vine copula &	\emph{14.5} &67.6	&-6.1	& 0.60 \\
TG log-kriging & 20.8 & 78.2 & -2.1 & 0.39 \\
residual kriging     & 21.1 & 75.6 & 5.2 & 0.43 \\
best one in SIC2004 & 14.9 & \emph{45.5} & \emph{-0.5} & \emph{0.84}  
\end{tabular}
\end{table}
\end{frame}

\begin{frame}{Reproduction of margins}
\begin{figure}
\includegraphics[width=\textwidth]{boxplots_emergency}
\end{figure}
\end{frame}

\begin{frame}{Uncertainty assessment}
\begin{figure}
\includegraphics[width=\textwidth]{prediction_cdf_large}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{prediction_cdf_small}
\end{figure}
\end{frame}

\begin{frame}{Simulation}
Predicting random quantiles from the spatial vine copula.

\begin{figure}
\includegraphics[width=\textwidth]{quantileSimWireFrame}
\end{figure}
\end{frame}

\section{Spatio-Temporal Copulas}

\begin{frame}{The spatio-temporal neighbourhood - the first tree}
\begin{figure}
\includegraphics[width=0.9\textwidth]{st-vine.png}
\end{figure}
\end{frame}

\begin{frame}{Spatio-temporal vine copulas}
The distribution of local neighbourhoods is decomposed into marginal distributions $F_i$ and a \alert{spatio-temporal vine copula}:
\begin{itemize}
\item The first tree is modelled as bivariate spatio-temporal copulas accounting for spatial and temporal distances.
\item Remaining trees are modelled from a wide set of "classical" bivariate copulas as a vine or truncated vine. 
\end{itemize}
\end{frame}

\subsection{Biv. Spatio-Temporal Copulas}
%
%\begin{frame}{Accounting for spatial distance}
%Thinking of pairs of spatio-temporal locations $\big((s_1,t_1), (s_2,t_2)\big)$ we assume \dots
%\begin{description}
%\item[distance] has a strong influence on the strength of dependence
%\item[dependence structure] is identical for all neighbours, but may change with distance
%\item[stationarity] and build $k$ bins by distance to estimate a bivariate copula $c_{j,h}(u,v)$ for all spatial bins $[0,l_1)$,$[l_1,l_2)$, \dots, $[l_{k-1},l_k)$ per temporal lag.
%\end{description}
%\end{frame}

\begin{frame}{Density of the bivariate spatio-temporal copula}
%The density of the \alert{bivariate spatial copula} is then given by a convex combination of bivariate copula densities:
%
%{\small
%$$ c^\Delta_{h} (u,v) := \left\{
%\begin{array}{ll}
%c^\Delta_{1,h}(u,v) &, 0 \leq h < l_1 \\
%(1-\lambda_2) c^\Delta_{1,h}(u,v) + \lambda_2 c^\Delta_{2,h}(u,v) &, l_1 \leq h < l_2 \\
%\vdots & \vdots \\
%(1-\lambda_k) c^\Delta_{k-1,h}(u,v) + \lambda_k\cdot 1 &, l_{k-1} \leq h < l_k \\
%1 &, l_k \leq h 
%\end{array}
%\right. $$ 
%} where \small{$ \lambda_j := \frac{h-l_{j-1}}{l_j-l_{j-1}} $}.
%\vspace{4pt}

The density of the \alert{bivariate spatio-temporal copula} $c_{h,\Delta}(u,v)$ is then given by a convex combination of bivariate spatial copula densities in an analogous manner.
\end{frame}

\subsection{Spatio-Temporal Vines}

\begin{frame}
\frametitle{The full density}
The remaining copulas $c_{j,j+i|0,\dots j-1}$ are estimated over the conditional sample. \vspace{4pt}

We get the full (here 10-dim) spatio-temporal vine copula density as a product of all involved bivariate densities:
\begin{align*}
 & c_{\mathbf{h},\mathbf{\Delta}}(u_{0},\dots,u_{9}) \\
=& \prod\limits_{i=1}^{9} c_{h,\Delta}(u_{0},u_i) \cdot \prod\limits_{j=1}^{9-1}\prod\limits_{i=1}^{9-j}c_{j,j+i|0,\dots,j-1} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1})
\end{align*}
%\small
%Where $u_0 = F\big(Z(s_0,t_0)\big), \dots, u_9=F\big(Z(s_3,t_{-2})\big)$,
%\begin{align*}
%u_{j|0}&=F_{h,\Delta}(u_j|u_0)=\frac{\partial C_{h,\Delta}(u_0,u_j)}{\partial u_0} \text{ and} \\ 
%u_{j+i|0,\dots, j-1}&=F_{j+i|0,\dots,j-1}(u_{j+i}|u_0,\dots, u_{j-1})\\
%&=\frac{\partial C_{j-1,j+i|0,\dots j-2}(u_{j-1|0,\dots j-2},u_{j+i|0,\dots j-2})}{\partial u_{j-1|0,\dots j-2}} 
%\end{align*}
\end{frame}

%\begin{frame}{A full 5-dimensional spatial vine copula}
%\begin{figure}
%\includegraphics[width=\textwidth]{can_vine.png}
%\end{figure}
%\end{frame}
%
%\begin{frame}{The full density}
%Eventually remaining trees with copulas $c_{j,j+i|0,\dots j-1}$ are estimated over the last spatially conditioned sample. \vspace{4pt}
%
%We get the full copula density as a product of all involved bivariate densities:
%\begin{align*}
%& c_{\mathbf{h}} (u_{0},\dots,u_{d}) \nonumber \\
%=& \prod\limits_{i=1}^{d} c_{h_0({i})}(u_0,u_i) \cdot \prod\limits_{j=1}^{l-1}\prod\limits_{i=1}^{d-j}c_{h_{j}(j+i)} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1}) \\
% & \cdot \prod\limits_{j=l}^{d-1}\prod\limits_{i=1}^{d-j}c_{j,j+i|0,\dots,j-1} (u_{j|0,\dots,j-1}, u_{j+i|0,\dots,j-1}) \nonumber
%\end{align*}
%\end{frame}

\begin{frame}{Spatio-temporal vine copula interpolation}
The estimate can be obtained as the expected value

$$ \widehat{Z}_m(s_0,t_0) = \int\limits_{[0,1]}  F^{-1}(u) \ c_{\mathbf{h},\mathbf{\Delta}}\big(u|u_1,\dots,u_d\big) \ \mathrm{d}u $$

or by calculating any percentile $p$ (i.e. the median)

$$ \widehat{Z}_p(s_0,t_0) = F^{-1}\big(C_{\mathbf{h},\mathbf{\Delta}}^{-1}(p|u_1,\dots,u_d)\big) $$

with the conditional density
$$
c_{\mathbf{h},\mathbf{\Delta}}(u|u_1,\dots,u_d) := \frac{c_{\mathbf{h},\mathbf{\Delta}}(u,u_1,\dots,u_d)}{\int_0^1 c_{\mathbf{h},\mathbf{\Delta}}(v, u_1, \dots, u_d) \mathrm{d}v}
$$
and $u_i = F_i\big(Z(s_i,t_i)\big)$.
\end{frame}

%\subsection{Software}
%
%\begin{frame}[fragile]{R-package spcopula}
%The developed methods are implemented in R and are available as package \href{http://r-forge.r-project.org/projects/spcopula/}{spcopula} at R-Forge. \vspace{4pt}
%
%The package spcopula extends and combines the R-packages \href{http://cran.r-project.org/web/packages/VineCopula/index.html}{VineCopula}, \href{http://cran.r-project.org/web/packages/spacetime/index.html}{spacetime} and \href{http://cran.r-project.org/web/packages/copula/index.html}{copula}.
%\end{frame}

\section{Application}
%\subsection{Joker}
%
%\begin{frame}{The joker data set}
%Part of the SIC 2004 \cite{Dubois2005a}; 200 (+ 808) locations with simulated radiation:
%\begin{figure}
%\includegraphics[width=\textwidth]{joker_hist.pdf}
%\end{figure}
%\end{frame}
%
%\begin{frame}[fragile,allowframebreaks]{The bivariate spatial copulas of the joker data set}
%
%{\footnotesize
%\begin{verbatim}
%> jokerBins <- calcBins(joker, "residRate", 
%+                       boundaries=2:10*1e4)
%> jokerCorFun0 <- fitCorFun(jokerBins, degree=1, weighted=T)
%> curve(jokerCorFun0, add=T)
%\end{verbatim}
%}
%
%\begin{figure}
%\includegraphics[width=\textwidth]{emergency_spatial_copula.pdf}
%\end{figure}
%
%\framebreak
%
%{\footnotesize
%\begin{verbatim}
%> llTau0 <- loglikByCopulasLags(jokerBins, 
%+                             calcCor=jokerCorFun0)
%> bestFitTau0 <- apply(llTau0$loglik, 1, which.max)
%> spCop0 <- spCopula(c(families[bestFitTau0[1:6]],
%+                      indepCopula()),
%+                    jokerBins$meanDists[1:7], jokerCorFun0)
%\end{verbatim}
%}
%
%\begin{figure}
%\includegraphics[width=\textwidth]{jokerSpCopula1.pdf}
%\end{figure}
%
%\framebreak
%
%{\footnotesize
%\begin{verbatim}
%> ## build the neighbourhood
%> jokerNeigh0 <- getNeighbours(joker, "residRate", size=10)
%> jokerNeigh0@data <- rankTransform(jokerNeigh0@data)
%
%> ## second tree
%> jokerCond1 <- dropSpTree(jokerNeigh0, spCop0)
%> jokerCondBins1 <- calcBins(jokerCond1, plot=T, cutoff=80000)
%> jokerCorFun1 <- fitCorFun(jokerCondBins1, degree=1, 
%+                           weighted=T)
%\end{verbatim}
%}
%
%Repeat these steps until the spatial influence vanishes \dots or until the vine is complete.
%\end{frame}
%
%\begin{frame}[fragile]{The spatial vine copula of the joker data set}
%
%{\footnotesize 
%\begin{verbatim}
%> spVine <- spVineCopula(list(spCop0, spCop1,
%+                                  spCop2, spCop3))
%> spVine
%Spatial vine copula family with only spatial tree(s). 
%Dimension:  5 
%
%> sum(dCopula(jokerNeigh5dim@data, spVine, 
%+             h=calcSpTreeDists(jokerNeigh5dim,4), log=T))
%[1] 118.2019
%\end{verbatim}
%}
%\end{frame}
%
%\begin{frame}[fragile,allowframebreaks]{Interpolation of the joker data set}
%
%{\footnotesize 
%\begin{verbatim}
%> predNeigh <- getNeighbours(joker, sic.test, "residRate", 
%+                            size=5, prediction=T)
%> predNeigh@data <- rankTransform(predNeigh@data)
%
%> spVineCopPred <- spCopPredict(predNeigh, spVine, 
%+                               list(q=qFun), "quantile")
%\end{verbatim}
%}
%
%\begin{table}
%\center\small
%\begin{tabular}{l|rrrr}
%approach & MAE & RMSE & ME & COR \\ \hline
%spatial vine copula &	\emph{14.5} &	\emph{67.6}	&-6.1	& \emph{0.60} \\
%TG log-kriging & 20.8 & 78.2 & \emph{-2.1} & 0.39 \\
%residual kriging     & 21.1 & 75.6 & 5.2 & 0.43 %\\
%% best one in SIC2004 & 14.9 & 45.5 & -0.5 & 0.84  
%\end{tabular}
%\end{table}
%
%\framebreak
%
%\begin{figure}
%\includegraphics[width=\textwidth]{qunatilePredWireFrame}
%\end{figure}
%\end{frame}

\subsection{Daily Mean $\rm{PM}_{10}$}

\begin{frame}{Daily mean $\rm{PM}_{10}$ concentrations across Europe}
Daily mean $\rm{PM}_{10}$ concentrations observed at 194 rural background stations across Europe for the year 2005. \vspace{4pt}

\begin{figure}
\includegraphics[width=\textwidth]{hist_margins.pdf}
\end{figure}
\end{frame}

\begin{frame}{The spatio-temporal neighbourhood}
\begin{figure}
\includegraphics[width=0.9\textwidth]{st-vine.png}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{The bivariate spatio-temporal copula}

{\footnotesize
\begin{verbatim}
> stBins <- calcStBins(EU_RB_2005,"rtPM10", nbins=40,
+                      t.lags=-(0:2), instances=NA,
+                      cor.method="fasttau", plot=F)
> calcKTau <- fitCorFun(stBins,c(3,3,3))
\end{verbatim}
}

\begin{figure}
\includegraphics[width=\textwidth]{correlogram.pdf}
\end{figure}

\framebreak

{\footnotesize
\begin{verbatim}
> loglikTau <- list()
> for(j in 1:3) {
+   tmpBins <- ... # j-th subset of stBins
+   loglikTau[[j]] <- loglikByCopulasLags(tmpBins, families,
+                                         calcKTau[[j]])
+ }
\end{verbatim}
}
The following families achieve the highest log-likelihood:
\tabcolsep 1.5pt
{\small
\begin{table}
\begin{tabular}{l|ccccccccccccccc}
distance & 25 & 61 & 99 & 139 & 177 & 216 & 255 & 294 & 334 & 373 & 412 & 451 & 491 & 529 & 569 \\ \hline
$\Delta = 0$ & t & F & t & F & F & F & F & F & F & F & F & F & F & F & F \\
$\Delta = -1$ &F & F & F & F & F & F & F & F & F & F & F & F & F & F & A \\
$\Delta = -2$ &F & F & F & F & F & F & A & A & A & A & A & A & A & A & A
\end{tabular}
\end{table}
}

\framebreak

Pick the best fitting copulas:

{\footnotesize
\begin{verbatim}
> stConvCop <- stCopula(components = listCops, 
+                       distances = listDists, 
+                       t.lags=c(0,-1,-2))
\end{verbatim}
}

\begin{figure}
\includegraphics[width=0.9\textwidth]{spCopula0.pdf}
\end{figure}

\framebreak

\begin{figure}
\includegraphics[width=0.9\textwidth]{spCopula1.pdf}

\includegraphics[width=0.9\textwidth]{spCopula2.pdf}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{The spatio-temporal vine copula}

Build the spatio-temporal neighbourhood and fit the upper vine:
{\footnotesize
\begin{verbatim}
> stNeigh <- getStNeighbours(EU_RB_2005, var="rtPM10",
+                            spSize=4, t.lags=-(0:2),
+                            timeSteps=90, min.dist=10)
> stVineFit <- fitCopula(stVineCopula(stConvCop, 
+                                     vineCopula(9L)),
+                        stNeigh, method="indeptest")
> stVineFit@loglik
[1] 72417.53
> stVine <- stVineFit@copula
> stVine
Spatio-temporal vine copula family with 1 spatio-temporal tree. 
Dimension:  10 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]{Interpolation of the air qualities}

Predictions can be obtained through

{\footnotesize 
\begin{verbatim}
> predNeigh <- getStNeighbours(EU_RB_2005, targetGeom, 
+                            "rtPM10", spSize=4, prediction=T)

> stVinePred <- stCopPredict(predNeigh, stVine, 
+                               list(q=qFun), "quantile")
\end{verbatim}
}

\end{frame}

\begin{frame}
\frametitle{Cross validation results for the daily mean $\rm{PM}_{10}$ interpolation}
\begin{table}
\center
\begin{tabular}{l|ccc}
& RMSE & bias & MAE \\ \hline
expected value $\widehat{Z}_m$ & 11.20 & -0.73 & 6.95 \\
median $\widehat{Z}_{0.5}$ & 12.08 & 1.94 & 6.87 \\ \hline
metric cov. kriging & 10.69 & -0.29 & 6.28 \\
metric cov. res. kriging & 10.67 & -0.47 & 6.16
\end{tabular}
\caption{Cross validation results for the expected value and median estimates following the vine copula approach and two methods from a comparison study on spatio-temporal kriging approaches in $\rm{PM}_{10}$ mapping.}
\end{table}
\end{frame}


\section{Discussion}
\begin{frame}{Second tree's correlations}
\begin{figure}
\includegraphics[width=\textwidth]{condCorDistBoxed}
\end{figure}
\end{frame}

\begin{frame}{Implicit modelled correlations}
\begin{figure}
\includegraphics[width=0.5\textwidth]{can_Vine_implicit.png}

\includegraphics[width=\textwidth]{CorDistBoxed}
\end{figure}
\end{frame}

\section{Conclusion \& Outlook}

\begin{frame}
\frametitle{Benefits}
\begin{description}
\item[richer flexibility] due to the various dependence structures
\item[asymmetric dependence structures] become possible (temporal direction)
\item[probabilistic advantage] sophisticated uncertainty analysis, drawing random samples, \dots
\end{description}
\end{frame}

\begin{frame}
\frametitle{Further extensions}
\begin{itemize}
\item including covariates \\
(e.g. altitude, population, EMEP, \dots)
\item flexible/complex neighbourhoods \\
	(e.g. by spatial direction, \dots)
\item larger neighbourhoods possibly using vine truncation techniques
\item include further copula families
\item improve performance
\end{itemize}
\end{frame}

\section{Tasks}
\begin{frame}{Tasks - In any case: ask your questions!}
\begin{enumerate}[a)]
\item take the \code{meuse} data set again, fit a marginal distribution
\item use the package \code{spcopula} to fit a bivariate spatial copula for the first tree
\item continue with the other trees/fit a static vine
\item predict for \code{meuse.grid}
\end{enumerate}
\end{frame}
\end{document}