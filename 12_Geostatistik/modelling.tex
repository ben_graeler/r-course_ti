\documentclass[compress,mathserif,table,href]{beamer}

\usepackage{animate}
\usepackage[numbers]{natbib}

\input{../talk-style.tex}

\graphicspath{{figures/}}

\lecture[]{Modelling}

\date{Thünen-Institut, Braunschweig, 18+19/25+26 Februar 2019}
\author{Dr. Benedikt Gräler}

\rowcolors{1}{}{}
\begin{document}

\begin{frame}
\maketitle
\end{frame}		

\begin{frame}{Meaningful Spatial Statistics}
Consider two data sets with columns $x$ and $y$ denoting location and a column ${\rm PM}_{10}$:

\begin{enumerate}[A)]
\item<1-> reporting annual ${\rm PM}_{10}$ emissions of all coal power plants in Germany (marked point pattern)
\item<1-> reporting annual average ${\rm PM}_{10}$ concentration at a set of measurement stations in Germany (sample of a random field)
\end{enumerate}
Both sets can (computationally) be summed and interpolated, but each of the operations makes only sense for either one of the data sets. 
\end{frame}

\section{Point Patterns}

\begin{frame}{Longleaf pine trees}
The following examples follow those from the spatstat package.\vspace{-16pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleaf}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Uniform intensity}
How many trees are there per square unit on average?
$$ \lambda=\frac{N}{area} $$
{\footnotesize
\begin{verbatim}
> summary(longleaf)
Marked planar point pattern:  584 points
Average intensity 0.0146 points per square metre

Coordinates are given to 1 decimal place
i.e. rounded to the nearest multiple of 0.1 metres

marks are  numeric,  of type ‘double’
Summary:
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   2.00    9.10   26.15   26.84   42.12   75.90 

Window: rectangle = [0, 200] x [0, 200] metres
Window area = 40000 square metres
Unit of length: 1 metre 
\end{verbatim}}
\end{frame}

\begin{frame}{Inhomogeneous intensity}
How does the intensity change throughout the study area?
\begin{description}
\item[quadrat counting] the region is split into areas of equal size and a uniform density is estimated per area
\item[kernel smoothing] the contribution of each point is spread across its neighbourhood based on some kernel density being properly rescaled
\end{description}
\end{frame}

\begin{frame}[fragile]{quadrat counting}
\vspace{-8pt}
\footnotesize
\begin{verbatim}
> quadratcount(longleaf,nx=4,ny=4)
           x
y           [0,40] (40,80] (80,120] (120,160] (160,200]
  (160,200]     20      25       37         7        26
  (120,160]     25      34       50        51        27
  (80,120]      29      22       15        31        37
  (40,80]       26      12       24        19         8
  [0,40]        18      14       12         8         7
\end{verbatim}\vspace{-28pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleafQcount}
\end{figure}

\end{frame}

\begin{frame}[fragile]{kernel smoothing}
\footnotesize
\begin{verbatim}
> density(longleaf)
\end{verbatim}
\vspace{-28pt}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{longleafDensity}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Fitting Poisson processes}
Following the algorithm, we seek a model where the intensity $\lambda$ is log-linear in the parameter $\theta$:
$$ \log\big(\lambda_{\theta}(x,y)\big) = \theta \cdot f(x,y)$$

A model fit with $f$ being a simple linear model of the coordinates, is obtained through
\footnotesize
\begin{verbatim}
> modelLl <- ppm(longleaf, ~x+y)
> modelLl
Nonstationary Poisson process

Trend formula: ~x + y
> AIC(modelLl)
6077
\end{verbatim}

\break

\normalsize
A model with f being a polynomial function can be fitted through:
\footnotesize
\begin{verbatim}
> modelLl <- ppm(ppLl, ~polynom(x, y, 3))
> modelLl
Nonstationary Poisson process

Trend formula: ~polynom(x, y, 3)
> AIC(modelLl)
6027
\end{verbatim}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{longleafTrend}
\end{figure}
\end{frame}

\begin{frame}[fragile]{The fitted Poisson process}
Now that the process is fitted, sampling can take place:
\footnotesize
\begin{verbatim}
rpoispp(lambdaFun, lmax=0.03, win=owin(c(0,200),c(0,200)))
\end{verbatim}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{longleafSim}
\end{figure}
\end{frame}

\begin{frame}{Additional options}
\begin{itemize}
\item intensity based on covariates (e.g. elevation)
\item intensity per category $\rightarrow$ multiple point process \break (e.g. sibling vs. adult)
\item interpoint interactions 
\item modelling of marks: (L, M\textbar L), (M, L\textbar M), (L,M)
\item testing for independence
\item GOF testing
\end{itemize}
\end{frame}

\section{Lattice}

\begin{frame}{Sudden infant death syndrome - North Carolina}
The following examples follow these of the spdep package and its vignettes.
\begin{figure}
\centering
\includegraphics[width=\textwidth]{BIRk_nc}
\end{figure}
\end{frame}

\begin{frame}{Expected counts - probability map}
Assuming Poisson distributions with mean values set to the expected number of cases $ec$ per county based on the number of births $ec(county) = N_{Birth} \cdot rate$, the cumulated density of the observed values is derived. 
\begin{figure}
\includegraphics[width=\textwidth]{nc_probabilityMap}
\end{figure}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{CAR models}
The motivation of a \emph{conditional autoregressive model} is given by the conditional distribution
\begin{eqnarray*}
& & f\big( z(s_i) | \{ z(s_j): j\neq i\}\big) \\
&= & \frac{1}{\tau_i \sqrt{2\pi}} \exp\left( \frac{-\big(z(s_i) - \theta_i(\{ z(s_j): j\neq i\}) \big)^2}{2 \tau_i^2} \right)
\end{eqnarray*}
with 
$$ \theta_i(\{ z(s_j): j\neq i\}) = \mu_i + \sum\limits_{j=1}^{n} c_{ij}\big( z(s_j)-\mu_j \big) $$
while $(c_{ij})_{ij}$ with $c_{ij}=0$ unless the locations $s_i$ and $s_j$ are pairwise dependent, $c_{ij}\tau^2_i = c_{ji}\tau^2_j$ and $c_{ii}=0$. $\tau^2_i$.

\break 

The process $Z$ can then be modelled as 
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-C)^{-1}M\big)$$
$$ Z(s_i) = \mu_i + \sum\limits_{j=1}^{n} c_{ij}\big( Z(s_j)-\mu_j \big) + \nu_i$$
with $\boldsymbol{\nu} \sim \rm{Gau}(\mathbf{0}, M\big(I-C^t)\big)$ while $M = \rm{diag}(\tau^2_1,\dots,\tau^2_n)$, $C=(c_{ij})_{ij}$.

Typically, only a small number of conditioning sites is used assuming the Markov property
$$  f\big( z(s_i) | \{ z(s_j): j\neq i\}\big) =  f\big( z(s_i) | \{ z(s_j): j \in N_i \}\big)$$ with $N_i$ being the index set of selected neighbouring locations.
\end{frame}

\begin{frame}[allowframebreaks,fragile]{CAR Example: SIDS in North Carolina}

Freeman-Tukey transformed data

\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_FTtransformed}
\end{figure}

$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-C)^{-1}M\big)$$
We assume a constant mean $\boldsymbol\mu=m$. The neighbourhood sets of the Markov Random Field are based on the distance between centroids of the counties ($\leq$~30~miles).
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_neighbourhoods}
\end{figure}

The spatial dependence matrix $C$ is modelled as
$$ c_{ij} := \left\{ 
\begin{array}{ll}
\phi \cdot \frac{\min_{j\in N_i, i=1,\dots,n}(d_{ij}) }{d_{ij}} \cdot \sqrt{\frac{n_j}{n_i}},& j \in N_i \\
0,& j\notin N_i
\end{array}\right. $$
and the conditional variance by
$$ \tau^2_i := \frac{\tau^2}{n_i} $$
such that $c_{ij}\tau^2_j = c_{ji}\tau^2i$ and $M=\rm{diag}(\tau^2_1,\dots,\tau^2_n)$.

The set of parameters is $(m,\phi,\tau)$.

\framebreak
The call 
\footnotesize
\begin{verbatim}
car <- spautolm(ft.SID74 ~ 1, data=mdata.4, 
                listw=sids.nhbr.listw.4,
                weights=BIR74, family="CAR")
summary(car)
\end{verbatim}
\normalsize
yields estimates:
$\hat{m} = 2.84$, $\hat{\phi}=1.73$ and $\hat{\tau}=36.16$

\framebreak

Predicted transformed data
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_SAR}
\end{figure}

\end{frame}

\begin{frame}[fragile]{SAR models}
A \emph{simultaneous autoregressive model} is given by
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-B)^{-1}\Lambda(I-B^t)^{-1}\big)$$
$$ Z(s_i) = \mu_i + \sum\limits_{j=1}^{n} b_{ij}\big( Z(s_j)-\mu_j \big) + \epsilon_i$$
with $\boldsymbol{\epsilon} \sim \rm{Gau}(\mathbf{0}, \Lambda\big)$, $B=(b_{ij})_{ij}$ while $b_{ii}=0$ and $b_{ij}$ captures the dependence of location $s_i$ on $s_j$. It is not necessarily $b_{ij}=b_{ji}$, but $(I-B)^{-1}$ is assumed to exist.
\end{frame}


\begin{frame}[allowframebreaks,fragile]{SAR Example: SIDS in North Carolina}

$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu},(I-B)^{-1}\Lambda(I-B^t)^{-1}\big)$$
We assume again a constant mean $\boldsymbol\mu=m$. 

The spatial dependence matrix $B$ is modelled as
$$ b_{ij} := \phi \frac{1}{d_{ij} \cdot \sum_{j \in N_i} \frac{1}{d_{ij}}} $$
and the variance by
$$ \sigma^2_i := \frac{\sigma^2}{n_i} $$
such that $\Lambda=\rm{diag}(\sigma^2_1,\dots,\sigma^2_n)$.

The set of parameters is $(m,\phi,\sigma)$.

\framebreak
The call 
\footnotesize
\begin{verbatim}
sar <- spautolm(ft.SID74 ~ 1, data=mdata.4, 
                listw=sids.nhbr.listw.4,
                weights=BIR74, family="SAR")
summary(sar)
\end{verbatim}
\normalsize
yields estimates:
$\hat{m} = 2.94$, $\hat{\phi}=0.8683$ and $\hat{\sigma}=35.55$

\framebreak

Predicted transformed data
\begin{figure}
\centering
\includegraphics[width=\textwidth]{nc_CAR}
\end{figure}

\end{frame}

\section{Spatial Fields}

\begin{frame}{Stationarity and Isotropy}
\begin{description}
\item<1->[stationarity] The process "looks" the same at each location (e.g. mean and variance do not change from east to west)
\item<1->[isotropy] The dependence between locations is determined only by their separating distance neglecting the direction (e.g. locations 2~km apart along the north-south axis are as correlated as stations 2~km apart along the east-west axis)
\end{description}

Some tricks exist to weaken these assumptions (e.g. rotating and rescaling coordinates).
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Variograms}
The dependence across space of a random field $Z$ is assessed using a \emph{variogram} $\gamma$:
$$ \gamma(h) = \frac{1}{2}\rm{E}\big(Z(s) - Z(s+h)\big)^2$$

the empirical estimator looks like 
$$ \hat{\gamma}(h) = \frac{1}{2 |N_h|} \sum\limits_{(i,j)\in N_h}\big(Z(s_i)-Z(s_j)\big)^2$$
while $N_h = \{(i,j) : h-\epsilon \leq ||s_i-s_j|| \leq h+\epsilon \}$\\

All R-code refers to the R-package \verb?gstat?.

\framebreak

The \emph{sample variogram} is obtained through
\footnotesize
\begin{verbatim}
vgmMeuse <- variogram(zinc~1, meuse)
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{meuseVgm}
\end{figure}

\framebreak
And a theoretical \emph{variogram model} can be fitted

\footnotesize
\begin{verbatim}
> head(vgm())
  short                        long
1   Nug                Nug (nugget)
2   Exp           Exp (exponential)
3   Sph             Sph (spherical)
4   Gau              Gau (gaussian)
5   Exc Exclass (Exponential class)
6   Mat                Mat (Matern)

> vgmModelMeuse <- fit.variogram(vgmMeuse, 
                                 vgm(0.6, "Sph", 1000, 0.1))
vgmModelMeuse
  model     psill    range
1   Nug  24813.21   0.0000
2   Sph 134753.99 831.2953
\end{verbatim}
\normalsize

\begin{figure}
\includegraphics[width=\textwidth]{meuseVgmModel}
\end{figure}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Kriging}
Certain variogram models can be used to parametrize a covariance matrix for a Gaussian random field over a finite set of locations $s_1$, \dots, $s_n$:
$$ Z \sim \rm{Gau}\big(\boldsymbol{\mu}, \Sigma\big) $$
while $\Sigma = (\sigma^2_{ij})_{ij}$ and $\sigma^2_{ij}=\sigma^2 - \gamma(||s_i-s_j||)$, $1 \leq i,j\leq n$ with $\sigma^2=\rm{Var}\big(Z(s)\big)$, $\boldsymbol{\mu}=(\mu_1,\dots,\mu_n)$.\vspace{4pt}

Predictions can be made using matrix inversion and matrix multiplications.

\framebreak
\footnotesize
\begin{verbatim}
krige(zinc~1, meuse, meuse.grid, model=vgmModelMeuse)
\end{verbatim}
\normalsize

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseObsKrige}
\end{figure}

\framebreak

The model quantifies how \emph{uncertain} it is about the estimates through the kriging variance:

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{meuseKrigeVar}
\end{figure}
\end{frame}

\begin{frame}{Overview of kriging types}
\begin{description}
\item[simple kriging] the mean value is known
\item[ordinary kriging] prediction based on coordinates 
\item[universal kriging] prediction based on coordinates and additional regressors (distance to the river)
\item[co-kriging] the cross-variogram between two variables is as well exploit (zinc and lead)
\end{description}
\end{frame}

\section{Spatio-Temporal Kriging}

\begin{frame}{Almost Spatio-Temporal approaches}
\begin{description}
\item[slice wise] the easiest adoption is to do interpolation per slice fitting a variogram model for each time slice
\item[pooled] the variogram is fitted based on all spatio-temporal data and is used to predict each time slice separately with the same model
\item[evolving] models mix the both extremes such that the variogram model adopts to the daily situation (e.g. in terms of overall variability, the sill) but range and the nugget/sill ratio depend on larger data samples.
\end{description}
\end{frame}

\begin{frame}
\frametitle{The spatio-temporal variogram}
Extending the variogram to a two-place function for spatio-temporal random fields $Z(s,t)$:
$$\gamma(h,u) = \rm{E}\big(Z(s,t)-Z(s+h,t+u) \big)^2$$
at any location $(s,t)$. And empirical version
$$ \hat{\gamma}(h,u) = \frac{1}{2 |N_{h,u}|} \sum\limits_{(i,j)\in N_{h,u}}\big(Z(s_i,t_i)-Z(s_j,t_j)\big)^2$$
while $N_{h,u} = \left\{(i,j) \left|\begin{array}{l}
h-\epsilon_s \leq ||s_i-s_j|| \leq h+\epsilon_s \\
u-\epsilon_t \leq t_i-t_j \leq u+\epsilon_t 
\end{array} \right.\right \}$
\end{frame}

\begin{frame}
\frametitle{Scenario}
We have a set of spatially spread time series of daily measurements and are asked to produce a map of means for the provided time frame.

\begin{figure}
\includegraphics[width=\textwidth]{stplot_ger_june.pdf}
\end{figure}
\end{frame}


\subsection{empirical}

\begin{frame}[fragile]
\frametitle{empirical spatio-temporal variogram surface}
The idea is the same as in the spatial case: binning of locations according to their separating distance. In the spatio-temporal case, distances are pairs of spatial and temporal distance yielding a variogram surface, not a single line.

\footnotesize
\begin{verbatim}
empVgm <- variogramST(PM10~1, ger_june, tlags=0:4,
                      cutoff=500e3)

# rescaling of distances
empVgm$dist <- empVgm$dist/1000
empVgm$spacelag <- empVgm$spacelag/1000

# wireframe:
plot(empVgm, wireframe=T, scales=list(arrows=F),
     col.regions=bpy.colors(),zlab=list(rot=90),zlim=c(0,20))

# levelplot:
plot(empVgm)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{empirical spatio-temporal variogram surface - wireframe}
\begin{figure}
\includegraphics[width=0.85\textwidth]{empStVgm_wf.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{empirical spatio-temporal variogram surface - levelplot}
\begin{figure}
\includegraphics[width=0.85\textwidth]{empStVgm.pdf}
\end{figure}
\end{frame}

\subsection{metric}

\begin{frame}
\frametitle{metric kriging}
The \alert{metric kriging} follows the natural idea of extending the 2-dimensional geographic space into a 3-dimensional spatio-temporal one. In order to achieve an isotropic space, the temporal domain has to be rescaled to match the spatial one (spatio-temporal anisotropy correction $\kappa$). \linebreak

All spatial, temporal and spatio-temporal distances are treated equally resulting in a joint covariance model $C_j$: 
$$C_m(h,u)=C_j\big(\sqrt{h^2+(\kappa\cdot u)^2}\big)$$
The variogram evaluates to
$$ \gamma_m(h,u) = \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2}) $$
where $\gamma_j$ is any known variogram including some nugget effect.
\end{frame}


\begin{frame}[fragile]
\frametitle{metric kriging in R}

\footnotesize
\begin{verbatim}
metricModel <- vgmST("metric", 
                     joint=vgm(0.8,"Exp", 150, 0.2),
                     stAni=100)
metricFit <- fit.StVariogram(empVgm,metricModel,
                             lower=c(0,10,0,10))

attr(metricFit,"optim.output")$value
plot(empVgm, metricFit)

predMetric <- krigeST(PM10~1, ger_june, 
                      STF(ger_gridded,tgrd), 
                      metricFit)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{metric spatio-temporal variogram surface}
\begin{figure}
\includegraphics[width=0.85\textwidth]{metricStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 15 - metric model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{kriged_metric_ger_june-15.pdf}
\end{figure}
\end{frame}


\subsection{separable}

\begin{frame}{separable covariance models}

A \alert{separable covariance function} is assumed to fulfill  $C_{sep}(h,u)=C_s(h)C_t(u)$. This is in general a rather strong simplification. Its variogram is given by 
$$\gamma_{sep}(h,u) = {\rm nug}\cdot {\bf1}_{h>0,u>0} + {\rm sill} \cdot \big( \gamma_s(h)+\gamma_t(u)-\gamma_s(h)\gamma_t(u) \big)$$
where $\gamma_s$ and $\gamma_t$ are spatial and temporal variograms without nugget effect and a sill of 1. The overall nugget and sill parameters are denoted by "$\rm nug$" and "$\rm sill$" respectively.
\end{frame}

\begin{frame}[fragile]
\frametitle{separable covariance model in R}
\footnotesize
\begin{verbatim}
sepModel <- vgmST("separable", 
                  space=vgm(0.8,"Exp", 150, 0.2),
                  time =vgm(0.7,"Exp", 6, 0.3),
                  sill=18)
sepFit <-  fit.StVariogram(empVgm,sepModel,
                           lower=c(10,0,1,0,0))

attr(sepFit,"optim.output")$value
plot(empVgm, sepFit)

predSep <- krigeST(PM10~1, ger_june, 
                   STF(ger_gridded,tgrd), 
                   sepFit)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram surface of the separable model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{sepStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 15 - seperable covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{kriged_separable_ger_june-15.pdf}
\end{figure}
\end{frame}


\subsection{product-sum}

\begin{frame}
\frametitle{product-sum covariance model}
The \alert{product sum covariance model} extends the simplifying assumption of the separable covariance model to:
$$C_{ps}(h,u)=C_s(h) + C_t(u) + k C_s(h)C_t(u)$$
with $k>0$ to fulfil the positive-definite condition. The corresponding variogram can be written as
$$\gamma_{\rm ps}(h,u) = (k \cdot sill_{\rm t} + 1) \gamma_{\rm s}(h) + (k \cdot sill_{\rm s} + 1) \gamma_{\rm t}(u) - k \gamma_{\rm s}(h) \gamma_{\rm t}(u)$$
where $\gamma_s$ and $\gamma_t$ are spatial and temporal variograms with nugget effects.
\end{frame}

\begin{frame}[fragile]
\frametitle{product-sum covariance model in R}
\footnotesize
\begin{verbatim}
empVgm$avgDist <- empVgm$avgDist/100
empVgm$spacelag <- empVgm$spacelag/100

psModel <- vgmST("productSum", 
                  space=vgm(4, "Exp", 6, 0.1),
                  time =vgm(5,"Sph", 6, 0.1),
                  k=0.8)
psFit <-  fit.StVariogram(empVgm,psModel)
attr(psFit,"optim.output")$value
plot(empVgm, psFit)

predPs <- krigeST(PM10~1, ger_june, 
                  STF(ger_gridded,tgrd), 
                  psFit)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram of the product-sum model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{psStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 15 - product-sum covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{kriged_productSum_ger_june-15.pdf}
\end{figure}
\end{frame}


\subsection{sum-metric}

\begin{frame}
\frametitle{sum-metric covariance model}
The \alert{sum-metric covariance model} is given by:
$$C_{sm}(h,u)=C_s(h)+C_t(u)+C_j\big(\sqrt{h^2+(\kappa\cdot u)^2}\big)$$ 
Originally, this model allows for spatial, temporal and joint nugget effects, a simplified version may allow only for a joint nugget. The non-simplified variogram is given by
$$ \gamma_{sm}(h,u)=  \gamma_s(h) + \gamma_t(u) + \gamma_j(\sqrt{h^2+(\kappa\cdot u)^2})$$
where $\gamma_s$, $\gamma_t$ and $\gamma_j$ are spatial, temporal and joint variograms with a separate nugget-effect.
\end{frame}

\begin{frame}[fragile]
\frametitle{sum-metric covariance model in R}
\footnotesize
\begin{verbatim}
empVgm$dist <- empVgm$dist/10
empVgm$spacelag <- empVgm$spacelag/10

sumMetricModel <- vgmST("sumMetric", 
                        space=vgm(5,"Exp",10,2),
                        time =vgm(5,"Exp", 6,2),
                        joint=vgm(5,"Exp",10,2),
                        stAni=10)
sumMetricFit <-  fit.StVariogram(empVgm,sumMetricModel,
                                 lower=c(0,1,0,0,1,0,0))
attr(sumMetricFit,"optim.output")$value

plot(empVgm, sumMetricFit)

predSumMetric <- krigeST(PM10~1, ger_june, 
                         STF(ger_gridded,tgrd), 
                         sumMetricFit)
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{variogram of the sum-metric model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{smStVgm.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriged map for day 15 - product-sum covariance model}
\begin{figure}
\includegraphics[width=0.85\textwidth]{kriged_sumMetric_ger_june-15.pdf}
\end{figure}
\end{frame}

\begin{frame}{variogram of all spatio-temporal models}
\begin{figure}
\includegraphics[width=\textwidth]{allStVgm}
\end{figure}
\end{frame}

\begin{frame}{Difference between empirical and spatio-temporal models}
\begin{figure}
\includegraphics[width=\textwidth]{allDiffVgm}
\end{figure}
\end{frame}

\begin{frame}{kriged map for 10 days}
\begin{figure}
\includegraphics[width=\textwidth]{predSt}
\end{figure}
\end{frame}

\begin{frame}
\graphicspath{{figures/}}
\center
\animategraphics[loop,scale=0.5,poster=first,autoplay]{2}{animate/pred}{1}{166}
\graphicspath{{figures/}}
\end{frame}


\subsection{Cross-validation}

\begin{frame}{Cross-validation}
In order to assess the models' prediction quality; either a set of stations is held back from the beginning, or every time series is left out in turn (Leave-One-Out-Cross-Validation). Cross-validation statistics are calculated in order to compare between the models. Note that the best variogram surface does not necessarily yield the best cross-validation.
\end{frame}

\begin{frame}{Cross-validation table}
\begin{table}
\small
\centering
\begin{tabular}{lrr|rrrr}
covariance model & & neigh. &  RMSE   &   MAE   &    ME   &   COR \\ \hline
pure Spatial & & 10 & 6.17 & 4.10 & -0.05 & 0.84 \\
separable &\hspace{-\tabcolsep}[5.41]&     10 & 6.09 & 4.05 & -0.01 & 0.84  \\ 
product-sum &\hspace{-\tabcolsep}[4.61]& 10 & 6.45 & 4.28 & -0.04 & 0.82  \\ 
metric &\hspace{-\tabcolsep}[6.24]&          10 & 6.11 & 4.08 &  0.02 & 0.84  \\ 
sum-metric &\hspace{-\tabcolsep}[3.57]&   10 & 6.19 & 4.11 & -0.06 & 0.84  \\ 
simple sum-metric &\hspace{-\tabcolsep}[3.58]& 10 &6.22 & 4.11 & -0.06 & 0.84 \\ \hline

pure Spatial & & 50 & 6.15 & 4.11 & -0.01 & 0.84  \\
separable &\hspace{-\tabcolsep}[5.41]& 50     & 6.05 & 4.04 & 0.00 & 0.84  \\ 
product-sum &\hspace{-\tabcolsep}[4.61]& 50 & 6.40 & 4.26 & -0.06 & 0.83  \\ 
metric &\hspace{-\tabcolsep}[6.24]& 50          & 6.04 & 4.06 & 0.05 & 0.85  \\ 
sum-metric &\hspace{-\tabcolsep}[3.57]& 50   & 6.18 & 4.11 & -0.03 & 0.84  \\ 
simple sum-metric &\hspace{-\tabcolsep}[3.58]& 50 & 6.18 & 4.11 & -0.04 & 0.84
\end{tabular}
\end{table}
\end{frame}

\section{spatio-temporal block kriging}
\begin{frame}
\frametitle{block kriging over time}
In the above scenario and with the presented methods, it is hard to get an uncertainty estimate of the temporally averaged value. Block kriging, with blocks over time, is one way to get such estimates. However, one has to decide on a model beforehand. Here, we will use the metric model again. \linebreak

Block kriging does not provide estimates for single locations but for areas or volumes. It has the property of providing the correct kriging variance for the block estimate that is typically lower due to the larger area. 
\end{frame}

\begin{frame}
\frametitle{monthly mean concentration - block kriged}
\begin{figure}
\includegraphics[scale=.6]{mean_concentration}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{block kriging variance}
\begin{figure}
\includegraphics[scale=.6]{block_variance}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{kriging variance day 15}
\begin{figure}
\includegraphics[scale=.6]{kriging_variance_day_15}
\end{figure}
\end{frame}

\begin{frame}[fragile]
\frametitle{block kriging in R - metric workaround}
\footnotesize
\begin{verbatim}
tmp_pred <- data.frame(cbind(ger_gridded@coords,15*tmpScale))
colnames(tmp_pred) <- c("x","y","t")
coordinates(tmp_pred) <- ~x+y+t
  
blockKrige <- krige(PM10~1, 
                    air3d[as.vector(!is.na(air3d@data)),],
                    newdata=tmp_pred, model=model3d, 
                    block=c(1,1,15*tmpScale))

ger_grid_time@sp@data <- blockKrige@data
\end{verbatim}
\end{frame}

\section{Local Spatio-Temporal Kriging}

\begin{frame}[fragile,allowframebreaks]{local spatio-temporal kriging}
Purely spatial kriging allows to select the n-nearest neighbours and use only these for prediction.
\linebreak

What does \alert{nearest} mean in a spatio-temporal context?
\linebreak

The idea is to select the most \alert{valuable} locations, i.e. the strongest correlated ones.
\linebreak

Simply set the argument \verb?nmax? and a local neighbourhood of the most correlated values is selected from a larger "metric" neighbourhood.

\begin{figure}
\includegraphics[width=0.9\textwidth]{vgmVsMetricDist}
\end{figure}
\end{frame}

\begin{frame}{Moving window local prediction}
In this approach, only time is selected from a local neighbourhood, but the entire spatial locations are used.\vspace{12pt}

Temporal blocks of $\pm$ temporal range are a useful temporal window.
\end{frame}


\section{Simulation}
\begin{frame}[fragile,allowframebreaks]{Simulation}
`gstat` bietet Simulation entlang eines zufälligen Pfades, aber auch über ein circulant embedding.

\footnotesize
\begin{verbatim}
library(sp)
library(gstat)
data("meuse")
coordinates(meuse) <- ~x+y

data("meuse.grid")
coordinates(meuse.grid) <- ~x+y
gridded(meuse.grid) <- TRUE

# variography
empVgm <- variogram(zinc~1, meuse)
modVgm <- fit.variogram(empVgm, vgm(150000, "Sph", 1000, 25000))

# unconditional simulation
unconSim <- krigeSimCE(zinc~1, newdata = meuse.grid, model = modVgm, n=100)
unconSim@data$zinc.simMean <- apply(unconSim@data[,-c(1:5)], 1, mean)

spplot(unconSim[,6:20], main="15 out of 100 unconditional simulations")
spplot(unconSim, "zinc.simMean", main="mean of 100 unconditional simulations")

# conditional simulation
conSim <- krigeSimCE(zinc~1, meuse, meuse.grid, modVgm, n=100)
conSim@data$zinc.simMean <- apply(conSim@data[,-c(1:5)], 1, mean)

spplot(conSim[,6:20], main="15 out of 100 conditional simulations")

# compare with kriging predictor
simKrige <- krige(zinc~1, meuse, meuse.grid, modVgm)
spplot(simKrige, "var1.pred", main="interpolated zinc concentrations")
spplot(conSim, "zinc.simMean", main="mean of 100 conditional simulations")
\end{verbatim}
\end{frame}

\section{Aufgaben}

\begin{frame}{Aufgaben}
\begin{enumerate}[a)]
\item schätze ein räumliches Variogram (z.B. für die \code{zinc} Konzentrationen des Meuse Datensatzes)
\item führe mehrer Interpolationen durch um die Schätzungen von idw, dem schlechtesten und besten Variogram Fit zu vergleichen.
\item lade den Datensatz \code{air} und wähle ein Zeitfenster von einem Monat
\item berechne das raum-zeitliche Variogram
\item passe eine Anzahl von raum-zeitlichen Modellen an  
\item interpoliere einige wenige Tage
\end{enumerate}
\end{frame}

\end{document}
