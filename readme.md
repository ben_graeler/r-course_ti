## Kursunterlagen zum zweitägigen Kurs "R als GIS"

Die Unterlagen wurden Anfang 2019 zusammengestellt und aufbereitet für einen zweitägigen Kurs am Thünen-Institut in Braunschweig. 

Teile der Unterlagen beruhen auf Vignetten und weiteren Materialien der vorgestellten/verwendeten R-Pakete. 

Über Folgende Links können die kompilierten pdfs und hands-on Skripte heruntergeladen werden. Die Quelldateien finden sich ebenfalls im Repository. 

- [Intro](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/00_Intro/intro.pdf)

- [Einführung in R](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/01_Einf%C3%BChrung%20in%20R/01_slides.pdf)

- [räumliche Daten](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/02_r%C3%A4umliche%20Daten/02_slides.pdf)
- [hands-on sp](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/02_r%C3%A4umliche%20Daten/hands-on_sp.R)
- [hands-on spacetime](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/02_r%C3%A4umliche%20Daten/hands-on_spacetime.R)

- [Karten Teil 1](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/03_Karten_I/03_slides.pdf)

- [Karten Teil 2](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/04_Karten_II/04_slides.pdf)

- [Interoperabilität](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/11_Interoperability/11_slides.pdf)

- [Geostatistik - pdf](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/12_Geostatistik/modelling.pdf)
- [hands-on gstat](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/12_Geostatistik/hands-on_gstat.R)
- [hands-on Lattice](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/12_Geostatistik/hands-on_Lattice.R)
- [hands-on point pattern](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/12_Geostatistik/hands-on_point_pattern.R)

- [Copulas](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/13_copulas/copulas.pdf)
- [hands-on copulas](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/13_copulas/hands-on_copula.R)

- [Spatial copulas](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/13_copulas/spatial_copulas.pdf)
- [hands-on spcopula](https://bitbucket.org/ben_graeler/r-course_ti/raw/master/13_copulas/hands-on_spcopula.R)

Ein Handout des gesamten Kurses lässt sich unter [R_als_GIS.pdf](https://bitbucket.org/ben_graeler/r-course_ti/raw/single_pdf/_book/R_als_GIS.pdf) herunterladen.

Dieses Repository trägt die Lizenz CC BY-SA.